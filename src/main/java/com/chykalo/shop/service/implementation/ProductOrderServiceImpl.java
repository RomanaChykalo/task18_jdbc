package com.chykalo.shop.service.implementation;

import com.chykalo.shop.dao.interfaces.ProductOrderDao;
import com.chykalo.shop.dao.implementation.ProductOrderDaoImpl;
import com.chykalo.shop.model.entities.ProductOrder;
import com.chykalo.shop.model.entities.ProductOrderPK;
import com.chykalo.shop.service.interfaces.ProductOrderService;

import java.sql.SQLException;
import java.util.List;

public class ProductOrderServiceImpl implements ProductOrderService {
    ProductOrderDao productOrderDao = new ProductOrderDaoImpl();

    @Override
    public List<ProductOrder> findAll() throws SQLException {
        return productOrderDao.findAll();
    }

    @Override
    public List<ProductOrder> findAverageProductsInOrderQuantity() throws SQLException {
        return productOrderDao.findAverageProductsInOrderQuantity();
    }

    @Override
    public int create(ProductOrder entity) throws SQLException {
        return productOrderDao.create(entity);
    }

    @Override
    public int update(ProductOrder entity) throws SQLException {
        return productOrderDao.update(entity);
    }

    @Override
    public int delete(ProductOrderPK id) throws SQLException {
        return productOrderDao.delete(id);
    }
}
