package com.chykalo.shop.service.implementation;

import com.chykalo.shop.dao.interfaces.ProductDao;
import com.chykalo.shop.dao.implementation.ProductDaoImpl;
import com.chykalo.shop.model.entities.Product;
import com.chykalo.shop.service.interfaces.ProductService;

import java.sql.SQLException;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    ProductDao productDao = new ProductDaoImpl();

    @Override
    public List<Product> findAll() throws SQLException {
        return productDao.findAll();
    }

    @Override
    public List<Product> findByPriceLowerThen50() throws SQLException {
        return productDao.findByPriceLowerThen50();
    }

    @Override
    public List<Product> findByPriceHigherThen50() throws SQLException {
        return productDao.findByPriceHigherThen50();
    }

    @Override
    public int create(Product entity) throws SQLException {
        return productDao.create(entity);
    }

    @Override
    public int update(Product entity) throws SQLException {
        return productDao.update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return productDao.delete(id);
    }

    @Override
    public List<Product> findProductWithMaxPrice() throws SQLException {
        return productDao.findProductWithMaxPrice();
    }
}
