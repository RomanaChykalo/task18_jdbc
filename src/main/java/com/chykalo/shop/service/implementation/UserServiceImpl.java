package com.chykalo.shop.service.implementation;

import com.chykalo.shop.dao.interfaces.UserDao;
import com.chykalo.shop.dao.implementation.UserDaoImpl;
import com.chykalo.shop.model.entities.User;
import com.chykalo.shop.service.interfaces.UserService;

import java.sql.SQLException;
import java.util.List;

public class UserServiceImpl implements UserService {
UserDao userDao = new UserDaoImpl();
    @Override
    public List<User> findAll() throws SQLException {
        return userDao.findAll();
    }

    @Override
    public User findById(Integer id) throws SQLException {
        User res = userDao.findById(id);
        return res;
    }

    @Override
    public int create(User entity) throws SQLException {
        return userDao.create(entity);
    }

    @Override
    public int update(User entity) throws SQLException {
        return userDao.update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return userDao.delete(id);
    }

    @Override
    public List<User> findByName(String name) throws SQLException {
        return userDao.findByName(name);
    }

    @Override
    public List<User> findBySurname(String name) throws SQLException {
        return userDao.findBySurname(name);
    }
}
