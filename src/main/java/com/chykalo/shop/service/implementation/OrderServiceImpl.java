package com.chykalo.shop.service.implementation;

import com.chykalo.shop.dao.interfaces.OrderDao;
import com.chykalo.shop.dao.implementation.OrderDaoImpl;
import com.chykalo.shop.model.entities.Order;
import com.chykalo.shop.service.interfaces.OrderService;

import java.sql.SQLException;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    OrderDao orderDao = new OrderDaoImpl();

    @Override
    public List<Order> findAll() throws SQLException {
        return orderDao.findAll();
    }

    @Override
    public Order findById(Integer id) throws SQLException {
        Order res = orderDao.findById(id);
        return res;
    }

    @Override
    public int create(Order order) throws SQLException {
        return orderDao.create(order);
    }

    @Override
    public int update(Order order) throws SQLException {
        return orderDao.update(order);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return orderDao.delete(id);
    }
}
