package com.chykalo.shop.service.interfaces;

import com.chykalo.shop.model.entities.ProductOrder;
import com.chykalo.shop.model.entities.ProductOrderPK;

import java.sql.SQLException;
import java.util.List;

public interface ProductOrderService {
    List<ProductOrder> findAll() throws SQLException;
    List<ProductOrder> findAverageProductsInOrderQuantity() throws SQLException;
    int create(ProductOrder entity) throws SQLException;
    int update(ProductOrder entity) throws SQLException;
    int delete(ProductOrderPK id) throws SQLException;
}
