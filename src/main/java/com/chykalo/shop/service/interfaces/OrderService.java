package com.chykalo.shop.service.interfaces;

import com.chykalo.shop.model.entities.Order;

import java.sql.SQLException;
import java.util.List;

public interface OrderService {
    List<Order> findAll() throws SQLException;

    Order findById(Integer id) throws SQLException;

    int create(Order order) throws SQLException;

    int update(Order order) throws SQLException;

    int delete(Integer id) throws SQLException;
}
