package com.chykalo.shop.service.interfaces;

import com.chykalo.shop.model.entities.User;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    List<User> findAll() throws SQLException;

    User findById(Integer id) throws SQLException;

    int create(User entity) throws SQLException;

    int update(User entity) throws SQLException;

    int delete(Integer id) throws SQLException;

    List<User> findByName(String name) throws SQLException;

    List<User> findBySurname(String name) throws SQLException;
}
