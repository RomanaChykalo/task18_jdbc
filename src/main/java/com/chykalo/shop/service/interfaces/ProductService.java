package com.chykalo.shop.service.interfaces;

import com.chykalo.shop.model.entities.Product;

import java.sql.SQLException;
import java.util.List;

public interface ProductService {
    List<Product> findAll() throws SQLException;

    List<Product> findByPriceLowerThen50() throws SQLException;

    List<Product> findByPriceHigherThen50() throws SQLException;

    int create(Product entity) throws SQLException;

    int update(Product entity) throws SQLException;

    int delete(Integer id) throws SQLException;

    List<Product> findProductWithMaxPrice() throws SQLException;
}
