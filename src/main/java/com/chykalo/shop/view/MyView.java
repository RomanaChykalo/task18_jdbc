package com.chykalo.shop.view;

import com.chykalo.shop.controller.OrderController;
import com.chykalo.shop.controller.ProductController;
import com.chykalo.shop.controller.ProductOrderController;
import com.chykalo.shop.controller.UserController;
import com.chykalo.shop.exceptions.DuplicateEntityException;
import com.chykalo.shop.exceptions.NoSuchEntityException;
import com.chykalo.shop.persistant.MetaData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_WAS_NOT_CREATED_EXC;
import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_DO_NOT_EXIST_EXC;
import static com.chykalo.shop.model.consts.menu.GeneralMenuConsts.*;
import static com.chykalo.shop.model.consts.menu.OrderMenuConsts.*;
import static com.chykalo.shop.model.consts.menu.ProductMenuConsts.*;
import static com.chykalo.shop.model.consts.menu.ProductMenuConsts.B;
import static com.chykalo.shop.model.consts.menu.ProductOrderMenuConsts.*;
import static com.chykalo.shop.model.consts.menu.UserMenuConsts.*;


public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;


    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put(A, TABLE_NAMES_LIST_MESSAGE);
        menu.put(B, PRODUCT_TABLE_STRUCTURE_MESSAGE);
        menu.put(C, ORDER_TABLE_STRUCTURE_MESSAGE);
        menu.put(D, PRODUCT_ORDER_TABLE_STRUCTURE_MESSAGE);

        menu.put(NUMBER_ONE, PRODUCT_TABLE_NAME);
        menu.put(NUMBER_ELEVEN, CREATE_PRODUCT_MESSAGE);
        menu.put(NUMBER_TWELVE, UPDATE_PRODUCT_MESSAGE);
        menu.put(NUMBER_THIRTEEN, DELETE_PRODUCT_MESSAGE);
        menu.put(NUMBER_FOURTEEN, SELECT_ALL_PRODUCT_MESSAGE);
        menu.put(NUMBER_FIFTEEN, SELECT_ALL_PRODUCT_CHEAPER_THEN_FIFTY_MESSAGE);
        menu.put(NUMBER_SIXTEEN, SELECT_ALL_PRODUCT_EXPENSIVE_THEN_FIFTY_MESSAGE);
        menu.put(NUMBER_SEVENTEEN, SELECT_THE_MOST_EXPENSIVE_PRODUCT_MESSAGE);

        menu.put(NUMBER_TWO, USER_TABLE_NAME);
        menu.put(NUMBER_TWENTY_ONE, CREATE_USER_MESSAGE);
        menu.put(NUMBER_TWENTY_TWO, UPDATE_USER_MESSAGE);
        menu.put(NUMBER_TWENTY_THREE, DELETE_USER_MESSAGE);
        menu.put(NUMBER_TWENTY_FOUR, SELECT_ALL_USER_MESSAGE);
        menu.put(NUMBER_TWENTY_FIVE, FIND_USER_BY_SURNAME);
        menu.put(NUMBER_TWENTY_SIX, FIND_USER_BY_NAME);
        menu.put(NUMBER_TWENTY_SEVEN, FIND_USER_BY_ID);

        menu.put(NUMBER_THREE, ORDER_TABLE_NAME_MESSAGE);
        menu.put(NUMBER_THIRTY_ONE, CREATE_ORDER_MESSAGE);
        menu.put(NUMBER_THIRTY_TWO, UPDATE_ORDER_MESSAGE);
        menu.put(NUMBER_THIRTY_THREE, DELETE_ORDER_MESSAGE);
        menu.put(NUMBER_THIRTY_FOUR, SELECT_ALL_ORDERS_MESSAGE);
        menu.put(NUMBER_THIRTY_FIVE, FIND_ORDER_BI_ID_MESSAGE);

        menu.put(NUMBER_FOUR, PRODUCT_ORDER_TABLE_NAME);
        menu.put(NUMBER_FORTY_ONE, CREATE_PRODUCT_ORDER_MESSAGE);
        menu.put(NUMBER_FORTY_TWO, UPDATE_PRODUCT_ORDER_MESSAGE);
        menu.put(NUMBER_FORTY_THREE, DELETE_PRODUCT_ORDER_MESSAGE);
        menu.put(NUMBER_FORTY_FOUR, SELECT_ALL_PRODUCT_ORDER_MESSAGE);
        menu.put(NUMBER_FORTY_FIVE, FIND_AVERAGE_PROD_QUANTITY_MESSAGE);

        methodsMenu.put(A, new MetaData()::printListOfTableNames);
        methodsMenu.put(B, new MetaData()::getProductTableColumnsInfo);
        methodsMenu.put(C, new MetaData()::getOrderTableColumnsInfo);
        methodsMenu.put(D, new MetaData()::getProductOrderTableColumnsInfo);

        methodsMenu.put(NUMBER_ELEVEN, new ProductController()::createProduct);
        methodsMenu.put(NUMBER_TWELVE, new ProductController()::updateProduct);
        methodsMenu.put(NUMBER_THIRTEEN, new ProductController()::deleteProduct);
        methodsMenu.put(NUMBER_FOURTEEN, new ProductController()::findAllProducts);
        methodsMenu.put(NUMBER_FIFTEEN, new ProductController()::findProductCheaperThenFifty);
        methodsMenu.put(NUMBER_SIXTEEN, new ProductController()::findProductExpensiveThenFifty);
        methodsMenu.put(NUMBER_SEVENTEEN, new ProductController()::findProductWithMaxPrice);

        methodsMenu.put(NUMBER_TWENTY_ONE, new UserController()::createUser);
        methodsMenu.put(NUMBER_TWENTY_TWO, new UserController()::updateUser);
        methodsMenu.put(NUMBER_TWENTY_THREE, new UserController()::deleteUser);
        methodsMenu.put(NUMBER_TWENTY_FOUR, new UserController()::findAllUsers);
        methodsMenu.put(NUMBER_TWENTY_FIVE, new UserController()::findUserBySurname);
        methodsMenu.put(NUMBER_TWENTY_SIX, new UserController()::findUserByName);
        methodsMenu.put(NUMBER_TWENTY_SEVEN, new UserController()::findUserById);

        methodsMenu.put(NUMBER_THIRTY_ONE, new OrderController()::createOrder);
        methodsMenu.put(NUMBER_THIRTY_TWO, new OrderController()::updateOrder);
        methodsMenu.put(NUMBER_THIRTY_THREE, new OrderController()::deleteOrderById);
        methodsMenu.put(NUMBER_THIRTY_FOUR, new OrderController()::findAllOrders);
        methodsMenu.put(NUMBER_THIRTY_FIVE, new OrderController()::findOrderByID);

        methodsMenu.put(NUMBER_FORTY_ONE, new ProductOrderController()::createProductOrderEntity);
        methodsMenu.put(NUMBER_FORTY_TWO, new ProductOrderController()::updateProductOrderEntity);
        methodsMenu.put(NUMBER_FORTY_THREE, new ProductOrderController()::deleteFromProductOrder);
        methodsMenu.put(NUMBER_FORTY_FOUR, new ProductOrderController()::selectAllProductOrderEntities);
        methodsMenu.put(NUMBER_FORTY_FIVE, new ProductOrderController()::findAverageProductsInOrderQuantity);
        menu.put(Q, EXIT);
    }

    private void outputMenu() {
        logger.info(MENU);
        for (String key : menu.keySet())
            if (key.length() == 1) logger.info(menu.get(key));
    }

    private void outputSubMenu(String fig) {

        logger.info(SUBMENU);
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) logger.info(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info(SELECT_MENU_POINT_MESSAGE);
            keyMenu = input.nextLine().toUpperCase();
            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                logger.info(SELECT_MENU_POINT_MESSAGE);
                keyMenu = input.nextLine().toUpperCase();
            }
            try {
                methodsMenu.get(keyMenu).print();
            } catch (NullPointerException e) {
            } catch (NoSuchEntityException e) {
                logger.error(ENTITY_DO_NOT_EXIST_EXC);
            } catch (ParseException e) {
                logger.error(ENTITY_DO_NOT_EXIST_EXC);
            } catch (SQLException | DuplicateEntityException e) {
                logger.error(ENTITY_WAS_NOT_CREATED_EXC);
            }
        } while (!keyMenu.equals(Q));
    }
}
