package com.chykalo.shop.persistant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.chykalo.shop.model.consts.menu.GeneralMenuConsts.*;

public class MetaData {

    private static Connection connection = ConnectionManager.getConnection();
    private static Logger logger = LogManager.getLogger(MetaData.class);
    private static DatabaseMetaData databaseMetaData;

    private List<String> getTableInfoList() throws SQLException {
        List<String> tableNames = new ArrayList<>();
        databaseMetaData = connection.getMetaData();
        ResultSet resultSet = databaseMetaData.getTables(connection.getCatalog(), null, "%",
                new String[]{TABLE_WORD});
        while (resultSet.next()) {
            tableNames.add(resultSet.getString(TABLE_NAME));
        }
        return tableNames;
    }

    public void printListOfTableNames() {
        List<String> tableNames = null;
        try {
            tableNames = getTableInfoList();
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
        logger.info(PRINT_TABLE_NAMES_MESSAGE);
        tableNames.stream().forEach(System.out::println);
    }

    private void getTableColumnsInfo(int tableNumber) throws SQLException {
        databaseMetaData = connection.getMetaData();
        ResultSet columns = databaseMetaData.getColumns(null, connection.getCatalog(),
                getTableInfoList().get(tableNumber),
                "%");
        logger.info(String.format(METADATA_TITLE_COLUMNS_FORMAT, COLUMN_NAME, TYPE_NAME, COLUMN_SIZE, IS_NULLABLE,
                IS_AUTOINCREMENT));
        while (columns.next()) {
            String columnName = columns.getString(COLUMN_NAME);
            String dataType = columns.getString(TYPE_NAME);
            String columnSize = columns.getString(COLUMN_SIZE);
            String isNullable = columns.getString(IS_NULLABLE);
            String is_autoIncrment = columns.getString(IS_AUTOINCREMENT);
            logger.info(String.format(METADATA_COLUMNS_FORMAT, columnName, dataType, columnSize, isNullable,
                    is_autoIncrment));
        }
    }

    public void getOrderTableColumnsInfo() throws SQLException {
        getTableColumnsInfo(0);
    }

    public void getProductTableColumnsInfo() throws SQLException {
        getTableColumnsInfo(1);
    }

    public void getProductOrderTableColumnsInfo() throws SQLException {
        getTableColumnsInfo(2);
    }
}