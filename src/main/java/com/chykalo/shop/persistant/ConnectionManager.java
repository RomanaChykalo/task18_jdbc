package com.chykalo.shop.persistant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

import static com.chykalo.shop.model.consts.ConnectionConsts.*;

public class ConnectionManager {
    private static Logger logger = LogManager.getLogger(ConnectionManager.class);
    private static Connection connection = null;
    private static Map<String, String> configList = new ConnectionManager().getInfoFromPropertyFile();

    private ConnectionManager() {
    }

    private Map<String, String> getInfoFromPropertyFile() {
        Properties prop = new Properties();
        try (InputStream input = getClass().getClassLoader().getResourceAsStream(PROPERTIES_PATH_NAME)) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        configList = new HashMap<>();
        configList.put(URL, prop.getProperty(URL));
        configList.put(USERNAME, prop.getProperty(USERNAME));
        configList.put(PASSWORD, prop.getProperty(PASSWORD));
        return configList;
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(configList.get(URL), configList.get(USERNAME),
                        configList.get(PASSWORD));
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        }
        return connection;
    }
}
