package com.chykalo.shop.transformer;

import com.chykalo.shop.model.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private final Class<T> clazz;
    private static Logger logger = LogManager.getLogger(Transformer.class);

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet resultSet) throws SQLException {
        Object model = null;
        try {
            model = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        if (fieldType == String.class) {
                            field.set(model, resultSet.getString(name));
                        } else if (fieldType == Integer.class) {
                            field.set(model, resultSet.getInt(name));
                        } else if (fieldType == Date.class) {
                            field.set(model, resultSet.getDate(name));
                        } else if (fieldType == BigDecimal.class) {
                            field.set(model, resultSet.getBigDecimal(name));
                        }
                    } else if (field.isAnnotationPresent(PrimaryKeyComposite.class)) {
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        Object FK = fieldType.getConstructor().newInstance();
                        field.set(model, FK);
                        Field[] fieldsInner = fieldType.getDeclaredFields();
                        for (Field fieldInner : fieldsInner) {
                            if (fieldInner.isAnnotationPresent(Column.class)) {
                                Column column = fieldInner.getAnnotation(Column.class);
                                String name = column.name();
                                fieldInner.setAccessible(true);
                                Class fieldInnerType = fieldInner.getType();
                                if (fieldInnerType == String.class) {
                                    fieldInner.set(FK, resultSet.getString(name));
                                } else if (fieldInnerType == Integer.class) {
                                    fieldInner.set(FK, resultSet.getInt(name));
                                } else if (fieldInnerType == Date.class) {
                                    fieldInner.set(FK, resultSet.getDate(name));
                                }
                            }
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage());
        } catch (InstantiationException e) {
            logger.error(e.getMessage());
        } catch (NoSuchMethodException e) {
            logger.error(e.getMessage());
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage());
        }
        return model;
    }
}
