package com.chykalo.shop.model.entities;

import com.chykalo.shop.model.annotation.Column;
import com.chykalo.shop.model.annotation.PrimaryKey;
import com.chykalo.shop.model.annotation.Table;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import static com.chykalo.shop.model.consts.entities.OrderConsts.*;
import static com.chykalo.shop.model.consts.entities.ProductConsts.PRODUCT_STRING_FORMAT;

@Table(name = ORDER_TABLE_NAME)
public class Order {
    @PrimaryKey
    @Column(name = ID_COLUMN_NAME, length = 11)
    private Integer id;
    private Set<Product> products = new HashSet<>();
    @Column(name = AMOUNT_COLUMN_NAME, length = 11)
    private Integer amount;
    @Column(name = USER_ID_COLUMN_NAME, length = 11)
    private Integer user_id;
    @Column(name = CREATED_COLUMN_NAME, length = 11)
    private Date date;

    public Order(Integer id, Integer amount, Integer user_id, Date date) {
        this.id = id;
        this.user_id = user_id;
        this.amount = amount;
        this.date = date;
    }

    public Order() {
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public BigDecimal calculatePrice() {
        BigDecimal sum = BigDecimal.ZERO;
        for (Product product : products) {
            sum = sum.add(product.getPrice());
        }
        return sum;
    }

    public Integer getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Set<Product> getProducts() {
        return products;
    }

    public Integer getAmount() {
        return amount;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Order order = (Order) object;

        if (id != order.id) return false;
        if (amount != order.amount) return false;
        return user_id == order.user_id;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + amount;
        result = 31 * result + user_id;
        return result;
    }

    @Override
    public String toString() {
        return String.format(ORDER_STRING_FORMAT, id, amount, user_id, date);
    }
}

