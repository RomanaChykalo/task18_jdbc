package com.chykalo.shop.model.entities;

import com.chykalo.shop.model.annotation.Column;

import static com.chykalo.shop.model.consts.entities.ProductOrderConsts.*;

public class ProductOrderPK {
    @Column(name = PRODUCT_ID_COLUMN_NAME, length = 11)
    private Integer productId;
    @Column(name = ORDER_ID_COLUMN_NAME, length = 11)
    private Integer orderId;

    public ProductOrderPK() {
    }

    public ProductOrderPK(Integer productId, Integer orderId) {
        this.productId = productId;
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        ProductOrderPK that = (ProductOrderPK) object;

        if (!productId.equals(that.productId)) return false;
        return orderId.equals(that.orderId);
    }

    @Override
    public int hashCode() {
        int result = productId.hashCode();
        result = 31 * result + orderId.hashCode();
        return result;
    }
}

