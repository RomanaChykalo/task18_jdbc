package com.chykalo.shop.model.entities;

import com.chykalo.shop.model.annotation.Column;
import com.chykalo.shop.model.annotation.PrimaryKeyComposite;
import com.chykalo.shop.model.annotation.Table;

import static com.chykalo.shop.model.consts.entities.ProductOrderConsts.*;

@Table(name = PRODUCT_ORDER_TABLE_NAME)
public class ProductOrder {
    @PrimaryKeyComposite
    private ProductOrderPK productOrderPK;
    @Column(name = PRODUCT_QUANTITY_COLUMN_NAME, length = 11)
    private Integer productQuantity;
    @Column(name = DISCOUNT_COLUMN_NAME, length = 11)
    private Integer discountPercent;

    public ProductOrder() {
    }

    public ProductOrder(ProductOrderPK productOrderPK, Integer productQuantity, Integer discountPercent) {
        this.productOrderPK = productOrderPK;
        this.productQuantity = productQuantity;
        this.discountPercent = discountPercent;
    }

    public ProductOrderPK getProductOrderPK() {
        return productOrderPK;
    }

    public void setProductOrderPK(ProductOrderPK productOrderPK) {
        this.productOrderPK = productOrderPK;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public Integer getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Integer discountPercent) {
        this.discountPercent = discountPercent;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        ProductOrder that = (ProductOrder) object;

        if (!productOrderPK.equals(that.productOrderPK)) return false;
        if (productQuantity != null ? !productQuantity.equals(that.productQuantity) : that.productQuantity != null)
            return false;
        return discountPercent != null ? discountPercent.equals(that.discountPercent) : that.discountPercent == null;
    }

    @Override
    public int hashCode() {
        int result = productOrderPK.hashCode();
        result = 31 * result + (productQuantity != null ? productQuantity.hashCode() : 0);
        result = 31 * result + (discountPercent != null ? discountPercent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(PRODUCT_ORDER_STRING_FORMAT, productOrderPK.getProductId(), productOrderPK.getOrderId(),
                productQuantity, discountPercent);
    }
}
