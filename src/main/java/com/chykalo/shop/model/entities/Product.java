package com.chykalo.shop.model.entities;

import com.chykalo.shop.model.annotation.Column;
import com.chykalo.shop.model.annotation.PrimaryKey;
import com.chykalo.shop.model.annotation.Table;

import java.math.BigDecimal;

import static com.chykalo.shop.model.consts.entities.ProductConsts.*;

@Table(name = PRODUCT_TABLE_NAME)
public class Product {
    @PrimaryKey
    @Column(name = ID_COLUMN_NAME, length = 11)
    private Integer id;
    @Column(name = NAME_COLUMN_NAME, length = 45)
    private String name;
    @Column(name = PRICE_COLUMN_NAME, length = 50)
    private BigDecimal price;
    @Column(name = COUNTRY_COLUMN_NAME, length = 45)
    private String country;

    public Product(String name, BigDecimal price, String country, Integer id) {
        this.name = name;
        this.price = price;
        this.country = country;
        this.id = id;
    }

    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Product product = (Product) object;

        if (id != product.id) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (price != null ? !price.equals(product.price) : product.price != null) return false;
        return country != null ? country.equals(product.country) : product.country == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(PRODUCT_STRING_FORMAT, id, name, price, country);
    }
}
