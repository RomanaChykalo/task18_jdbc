package com.chykalo.shop.model.entities;

import com.chykalo.shop.model.annotation.Column;
import com.chykalo.shop.model.annotation.PrimaryKey;
import com.chykalo.shop.model.annotation.Table;

import java.util.ArrayList;
import java.util.List;

import static com.chykalo.shop.model.consts.entities.UserConsts.*;

@Table(name = USER_TABLE_NAME)
public class User {
    @PrimaryKey
    @Column(name = ID_COLUMN_NAME, length = 11)
    private Integer id;
    @Column(name = NAME_COLUMN_NAME, length = 45)
    private String name;
    @Column(name = SURNAME_COLUMN_NAME, length = 45)
    private String surname;
    @Column(name = EMAIL_COLUMN_NAME, length = 45)
    private String email;
    @Column(name = PHONE_COLUMN_NAME, length = 11)
    private String phone;
    private List<Order> userOrders = new ArrayList();

    public User(Integer id, String name, String surname, String email, String phone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public User() {
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        User user = (User) object;

        if (id != user.id) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (surname != null ? !surname.equals(user.surname) : user.surname != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return phone != null ? phone.equals(user.phone) : user.phone == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(USER_STRING_FORMAT, id, name, surname, email, phone);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public List<Order> getUserOrders() {
        return userOrders;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUserOrders(List userOrders) {
        this.userOrders = userOrders;
    }
}

