package com.chykalo.shop.model.consts.menu;

public class ProductMenuConsts {
    private ProductMenuConsts() {}

    public static String B = "B";
    public static String NUMBER_ONE = "1";
    public static String NUMBER_ELEVEN = "11";
    public static String NUMBER_TWELVE = "12";
    public static String NUMBER_THIRTEEN = "13";
    public static String NUMBER_FOURTEEN = "14";
    public static String NUMBER_FIFTEEN = "15";
    public static String NUMBER_SIXTEEN = "16";
    public static String NUMBER_SEVENTEEN = "17";
    public static String PRODUCT_TABLE_NAME = "   1 - Table: Product";
    public static String CREATE_PRODUCT_MESSAGE = "  11 - Create new product";
    public static String UPDATE_PRODUCT_MESSAGE = "  12 - Update product";
    public static String DELETE_PRODUCT_MESSAGE = "  13 - Delete product";
    public static String SELECT_ALL_PRODUCT_MESSAGE = "  14 - Select all available products";
    public static String SELECT_ALL_PRODUCT_CHEAPER_THEN_FIFTY_MESSAGE = "  15 - Select all products cheaper then 50$";
    public static String SELECT_ALL_PRODUCT_EXPENSIVE_THEN_FIFTY_MESSAGE =
            "  16 - Select all products more expensive then 50$";
    public static String SELECT_THE_MOST_EXPENSIVE_PRODUCT_MESSAGE = "  17 - Select the most expensive product";
    public static String PRODUCT_TABLE_STRUCTURE_MESSAGE ="   B - Select structure of product table";
}
