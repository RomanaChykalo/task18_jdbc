package com.chykalo.shop.model.consts.menu;

public class ProductOrderMenuConsts {
    private ProductOrderMenuConsts(){}
    public static String PRODUCT_ORDER_TABLE_STRUCTURE_MESSAGE = "   D - Select structure of product order table";
    public static String D = "D";
    public static String NUMBER_FOUR = "4";
    public static String NUMBER_FORTY_ONE = "41";
    public static String NUMBER_FORTY_TWO = "42";
    public static String NUMBER_FORTY_THREE = "43";
    public static String NUMBER_FORTY_FOUR = "44";
    public static String NUMBER_FORTY_FIVE = "45";
    public static String PRODUCT_ORDER_TABLE_NAME ="   4 - Table: Product order";
    public static String CREATE_PRODUCT_ORDER_MESSAGE ="  41 - Create product in order entity";
    public static String UPDATE_PRODUCT_ORDER_MESSAGE ="  42 - Update product in order entity";
    public static String DELETE_PRODUCT_ORDER_MESSAGE ="  43 - Delete product in order entity";
    public static String SELECT_ALL_PRODUCT_ORDER_MESSAGE ="  44 - Select all product in order entities";
    public static String FIND_AVERAGE_PROD_QUANTITY_MESSAGE ="  45 - Find average product quantity";
}
