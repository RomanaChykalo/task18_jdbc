package com.chykalo.shop.model.consts.entities;

public class ProductOrderConsts {
    private ProductOrderConsts() {
    }

    public static final String PRODUCT_ORDER_TABLE_NAME = "product_order";
    public static final String PRODUCT_QUANTITY_COLUMN_NAME = "product_quantity";
    public static final String DISCOUNT_COLUMN_NAME = "discount_percent";
    public static final String PRODUCT_ID_COLUMN_NAME = "product_id";
    public static final String ORDER_ID_COLUMN_NAME ="order_id";
    public static final String PRODUCT_ORDER_STRING_FORMAT = "%-20s %-10s %-2s %16s";
}
