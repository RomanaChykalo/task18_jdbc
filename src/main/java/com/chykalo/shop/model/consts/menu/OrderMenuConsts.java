package com.chykalo.shop.model.consts.menu;

public class OrderMenuConsts {
    private OrderMenuConsts() {}
    public static String ORDER_TABLE_STRUCTURE_MESSAGE ="   C - Select structure of order table";
    public static String C = "C";
    public static String NUMBER_THREE="3";
    public static String ORDER_TABLE_NAME_MESSAGE = "   3 - Table: Order";
    public static String NUMBER_THIRTY_ONE = "31";
    public static String NUMBER_THIRTY_TWO = "32";
    public static String NUMBER_THIRTY_THREE = "33";
    public static String NUMBER_THIRTY_FOUR = "34";
    public static String NUMBER_THIRTY_FIVE = "35";
    public static String CREATE_ORDER_MESSAGE = "  31 - Create order";
    public static String UPDATE_ORDER_MESSAGE = "  32 - Update order";
    public static String DELETE_ORDER_MESSAGE = "  33 - Delete order";
    public static String SELECT_ALL_ORDERS_MESSAGE =  "  34 - Select all orders";
    public static String FIND_ORDER_BI_ID_MESSAGE = "  35 - Find order by ID";
}
