package com.chykalo.shop.model.consts.controller;

public class OrderControllerConsts {
    private OrderControllerConsts(){}
    public static final String ORDER_CONTROLLER_STRING_FORMAT = "%s %s %s %s";
    public static final String ORDER_ID = "order_id";
    public static final String ORDER_AMOUNT = "order_amount";
    public static final String ORDER_DATE = "date";
    public static final String DATE_PATTERN="yyyyMMdd";
}
