package com.chykalo.shop.model.consts.controller;

public class ProductControllerConsts {
    private ProductControllerConsts() {};
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "name";
    public static final String PRODUCT_PRICE = "price";
    public static final String PRODUCT_COUNTRY = "country_from";
    public static final String PRODUCT_CONTROLLER_STRING_FORMAT="%s %s %s %s";
}
