package com.chykalo.shop.model.consts;

public class InfoConsts {
    private InfoConsts() {
    }
    public static final String SEARCH_USER_BY_ID_MESSAGE = "Please, enter user's id you want to find";
    public static final String SEARCH_USER_BY_NAME_MESSAGE = "Please, enter user's name you want to find";
    public static final String SEARCH_USER_BY_SURNAME_MESSAGE = "Please, enter user's surname you want to find";
    public static final String ENTER_USER_ID_MESSAGE = "Enter Id for user: ";
    public static final String ENTER_USER_NAME_MESSAGE = "Enter user's name: ";
    public static final String ENTER_USER_SURNAME_MESSAGE = "Enter user's surname: ";
    public static final String ENTER_USER_PHONE_MESSAGE = "Enter user's phone: ";
    public static final String ENTER_USER_EMAIL_MESSAGE = "Enter user's email: ";
    public static final String SEARCH_ORDER_BY_ID_MESSAGE = "Please, enter order's id";
    public static final String ENTER_ORDER_DATE_MESSAGE = "Enter date, when order was created";
    public static final String ENTER_ORDER_ID_MESSAGE = "Enter order's id: ";
    public static final String ENTER_ORDER_AMOUNT_MESSAGE = "Enter order's amount: ";
    public static final String ENTER_PRODUCT_ID_MESSAGE = "Enter Id for product: ";
    public static final String ENTER_PRODUCT_NAME_MESSAGE = "Enter product's name: ";
    public static final String ENTER_PRODUCT_PRICE_MESSAGE = "Enter product's price: ";
    public static final String ENTER_PRODUCT_COUNTRY_FROM_MESSAGE = "Enter product's country: ";
    public static final String ENTER_DISCOUNT_MESSAGE = "Please, enter discount percent";
    public static final String ENTER_PRODUCT_QUANTITY_MESSAGE = "Please, enter products quantity in order";
}
