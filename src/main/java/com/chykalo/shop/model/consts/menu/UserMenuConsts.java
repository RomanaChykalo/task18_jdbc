package com.chykalo.shop.model.consts.menu;

public class UserMenuConsts {
    private UserMenuConsts(){}
    public static String NUMBER_TWO = "2";
    public static String NUMBER_TWENTY_ONE = "21";
    public static String NUMBER_TWENTY_TWO = "22";
    public static String NUMBER_TWENTY_THREE = "23";
    public static String NUMBER_TWENTY_FOUR = "24";
    public static String NUMBER_TWENTY_FIVE = "25";
    public static String NUMBER_TWENTY_SIX = "26";
    public static String NUMBER_TWENTY_SEVEN = "27";
    public static String USER_TABLE_NAME = "   2 - Table: User";
    public static String CREATE_USER_MESSAGE = "  21 - Create user";
    public static String UPDATE_USER_MESSAGE = "  22 - Update user";
    public static String DELETE_USER_MESSAGE = "  23 - Delete user";
    public static String SELECT_ALL_USER_MESSAGE = "  24 - Select all users";
    public static String FIND_USER_BY_SURNAME = "  25 - Find user by surname";
    public static String FIND_USER_BY_NAME = "  26 - Find user by name";
    public static String FIND_USER_BY_ID = "  27 - Find user by id";
}
