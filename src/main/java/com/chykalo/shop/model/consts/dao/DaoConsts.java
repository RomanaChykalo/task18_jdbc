package com.chykalo.shop.model.consts.dao;

public class DaoConsts {
    private DaoConsts() {
    }

    public static final String FIND_ALL_USERS = "SELECT * FROM user";
    public static final String DELETE_USER = "DELETE FROM user WHERE id=?";
    public static final String CREATE_USER = "INSERT user (id, name, surname, email, phone) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE_USER = "UPDATE user SET name=?, surname=?, email=?, phone=? WHERE id=?";
    public static final String USER_FIND_BY_ID = "SELECT * FROM user WHERE id=?";
    public static final String USER_FIND_BY_NAME = "SELECT * FROM user WHERE name=?";
    public static final String USER_FIND_BY_SURNAME = "SELECT * FROM user WHERE surname=?";
    public static final String FIND_ALL_ORDERS = "SELECT * FROM shop.order";
    public static final String DELETE_ORDER = "DELETE FROM shop.order WHERE id=?";
    public static final String CREATE_ORDER = "INSERT shop.order (id,amount,user_id,created) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_ORDER = "UPDATE shop.order SET user_id=?,amount=?,created=? WHERE id=?";
    public static final String ORDER_FIND_BY_ID = "SELECT * FROM shop.order WHERE id=?";
    public static final String FIND_ALL_PRODUCTS = "SELECT * FROM product";
    public static final String FIND_PRODUCTS_PRICE_LOWER_THEN_50 = "SELECT * FROM product where price<50";
    public static final String FIND_PRODUCTS_PRICE_HIGHER_THEN_50 = "SELECT * FROM product where price>50";
    public static final String DELETE_PRODUCT = "DELETE FROM product WHERE id=?";
    public static final String CREATE_PRODUCT = "INSERT product (name, price, country, id) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_PRODUCT = "UPDATE product SET name=?, price=?, country=? WHERE id=?";
    public static final String FIND_PRODUCTS_MAX_PRICE = "SELECT * FROM product where price=(SELECT MAX(price) FROM product)";
    public static final String FIND_BY_ORDER_ID_FROM_PRODUCT_ORDER = "SELECT * FROM product_order WHERE order_id=?";
    public static final String FIND_BY_USER_ID_FROM_PRODUCT_ORDER = "SELECT * FROM product_order WHERE user_id=?";
    public static final String FIND_ALL_PRODUCT_ORDER_ENTITIES = "SELECT * FROM product_order";
    public static final String CREATE_PRODUCT_ORDER_ENTITY = "INSERT product_order (product_id,order_id,product_quantity,discount_percent) VALUES (?, ?, ?, ?)";
    public static final String FIND_PRODUCT_AVERAGE_QUANTITY = "SELECT product_id, order_id, AVG(product_quantity) product_quantity,discount_percent FROM product_order GROUP BY product_id";
    public static final String UPDATE_PRODUCT_ORDER_ENTITY = "UPDATE product_order SET product_quantity=?, discount_percent=? WHERE product_id=? AND order_id=?";
    public static final String DELETE_PRODUCT_ORDER_ENTITY = "DELETE FROM product_order WHERE product_id=? AND order_id=?";
}
