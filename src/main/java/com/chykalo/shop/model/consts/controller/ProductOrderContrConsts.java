package com.chykalo.shop.model.consts.controller;

public class ProductOrderContrConsts {
    private ProductOrderContrConsts() {};
    public static final String PRODUCT_QUANTITY = "product_quantity";
    public static final String DISCOUNT_PERCENT = "discount_percent";
    public static final String PRODUCT_ORDER_CONTROLLER_STRING_FORMAT="%s %s %s %s";
    public static final String INPUT_PR_ID_DELETE_FROM_PR_ORDER_MESSAGE="Input product id for product in order entity be deleted: ";
    public static final String INPUT_ORDER_ID_DELETE_FROM_PR_ORDER_MESSAGE="Input order id for product in order entity be deleted: ";
}
