package com.chykalo.shop.model.consts;

public class ConnectionConsts {
    private ConnectionConsts() {
    }

    public static String PROPERTIES_PATH_NAME = "application.properties";
    public static String URL = "url";
    public static String USERNAME="username";
    public static String PASSWORD ="password";
}
