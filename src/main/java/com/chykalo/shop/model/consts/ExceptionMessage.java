package com.chykalo.shop.model.consts;

public class ExceptionMessage {
    private ExceptionMessage() {
    }
    public static String SQL_EXC_MESSAGE = " Can't read/write information from/to the database";
    public static final String ENTITY_WAS_NOT_CREATED_EXC = " Entity was not created, entity with such id already" +
            " exists, please, try again, enter correct data\n";
    public static final String ENTITY_DO_NOT_EXIST_EXC = " Entity was not deleted/updated/founded, entity with such id" +
            " does not exist, please, try again, and enter correct data\n";
}
