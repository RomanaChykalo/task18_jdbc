package com.chykalo.shop.model.consts.menu;

public class GeneralMenuConsts {
    private GeneralMenuConsts() {
    }
    public static String TABLE_WORD="TABLE";
    public static String TABLE_NAME="TABLE_NAME";
    public static String COLUMN_NAME="COLUMN_NAME";
    public static String TYPE_NAME="TYPE_NAME";
    public static String COLUMN_SIZE="COLUMN_SIZE";
    public static String IS_NULLABLE= "IS_NULLABLE";
    public static String IS_AUTOINCREMENT="IS_AUTOINCREMENT";
    public static String METADATA_TITLE_COLUMNS_FORMAT = "%-10s %-10s %s %s %-10s";
    public static String METADATA_COLUMNS_FORMAT = "%-20s %-10s %-10s %-10s %-10s";
    public static String MENU = "\nMENU:";
    public static String SUBMENU = "\nSubMENU:";
    public static String SELECT_MENU_POINT_MESSAGE = "Please, select menu point.";
    public static String Q = "Q";
    public static String EXIT = "   Q - exit";
    public static String A = "A";
    public static String TABLE_NAMES_LIST_MESSAGE = "   A - Print list of table's names from shop db";
    public static String EXIT_MESSAGE = "You finished program process";
    public static String PRINT_TABLE_NAMES_MESSAGE = "Printing all tables from db shop, type \"TABLE\" ";
}
