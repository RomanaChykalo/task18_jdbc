package com.chykalo.shop.model.consts.controller;

public class UserControllerConsts {
    private UserControllerConsts() {};
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_SURNAME = "user_surname";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PHONE = "user_phone";
    public static final String DELETED_ROWS_MESSAGE = "There was deleted 1 row\n";
    public static final String CREATED_ROWS_MESSAGE = "There was created 1 row\n";
    public static final String UPDATED_ROWS_MESSAGE = "There was updated 1 row\n";
    public static final String USER_CONTRL_STRING_FORMAT = "%s %s %-14s %-20s %-25s";
}
