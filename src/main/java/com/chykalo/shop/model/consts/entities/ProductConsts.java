package com.chykalo.shop.model.consts.entities;

public class ProductConsts {
    private ProductConsts() {
    }

    public static final String PRODUCT_TABLE_NAME = "product";
    public static final String ID_COLUMN_NAME = "id";
    public static final String NAME_COLUMN_NAME = "name";
    public static final String PRICE_COLUMN_NAME = "price";
    public static final String COUNTRY_COLUMN_NAME ="country";
    public static final String PRODUCT_STRING_FORMAT = "%-8s %-10s %-5s %-10s";
}
