package com.chykalo.shop.model.consts.entities;

public class UserConsts {
    private UserConsts() {
    }

    public static final String USER_TABLE_NAME = "user";
    public static final String ID_COLUMN_NAME = "id";
    public static final String NAME_COLUMN_NAME = "name";
    public static final String SURNAME_COLUMN_NAME = "surname";
    public static final String EMAIL_COLUMN_NAME ="email";
    public static final String PHONE_COLUMN_NAME ="phone";
    public static final String USER_STRING_FORMAT = "%-10s %-10s %-15s %-20s %-8s";
}
