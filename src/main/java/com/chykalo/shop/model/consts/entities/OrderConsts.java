package com.chykalo.shop.model.consts.entities;

public class OrderConsts {
    private OrderConsts() {
    }

    public static final String ORDER_TABLE_NAME = "order";
    public static final String ID_COLUMN_NAME = "id";
    public static final String AMOUNT_COLUMN_NAME = "amount";
    public static final String USER_ID_COLUMN_NAME = "user_id";
    public static final String CREATED_COLUMN_NAME = "created";
    public static final String ORDER_STRING_FORMAT = "%-20s %-10s %-2s %-10s";
}
