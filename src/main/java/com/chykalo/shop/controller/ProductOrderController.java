package com.chykalo.shop.controller;

import com.chykalo.shop.exceptions.NoSuchEntityException;
import com.chykalo.shop.model.entities.ProductOrder;
import com.chykalo.shop.model.entities.ProductOrderPK;
import com.chykalo.shop.service.interfaces.ProductOrderService;
import com.chykalo.shop.service.implementation.ProductOrderServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_DO_NOT_EXIST_EXC;
import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_WAS_NOT_CREATED_EXC;
import static com.chykalo.shop.model.consts.InfoConsts.*;
import static com.chykalo.shop.model.consts.controller.OrderControllerConsts.ORDER_ID;
import static com.chykalo.shop.model.consts.controller.ProductControllerConsts.*;
import static com.chykalo.shop.model.consts.controller.ProductOrderContrConsts.*;
import static com.chykalo.shop.model.consts.controller.UserControllerConsts.CREATED_ROWS_MESSAGE;
import static com.chykalo.shop.model.consts.controller.UserControllerConsts.DELETED_ROWS_MESSAGE;
import static com.chykalo.shop.model.consts.controller.UserControllerConsts.UPDATED_ROWS_MESSAGE;

public class ProductOrderController {
    private static Logger logger = LogManager.getLogger(ProductOrderController.class);
    private static Scanner input = new Scanner(System.in);
    ProductOrderService productOrderService = new ProductOrderServiceImpl();

    public void deleteFromProductOrder() throws SQLException {
        logger.info(INPUT_PR_ID_DELETE_FROM_PR_ORDER_MESSAGE);
        Integer productId = input.nextInt();
        input.nextLine();
        logger.info(INPUT_ORDER_ID_DELETE_FROM_PR_ORDER_MESSAGE);
        Integer orderId = input.nextInt();
        ProductOrderPK pk = new ProductOrderPK(productId, orderId);
        int count = productOrderService.delete(pk);
        if (count > 0) {
            logger.info(DELETED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    private ProductOrder setProductOrderInfo() {
        logger.info(ENTER_PRODUCT_ID_MESSAGE);
        Integer productId = input.nextInt();
        input.nextLine();
        logger.info(ENTER_ORDER_ID_MESSAGE);
        Integer orderId = input.nextInt();
        logger.info(ENTER_PRODUCT_QUANTITY_MESSAGE);
        Integer productQuantity = input.nextInt();
        logger.info(ENTER_DISCOUNT_MESSAGE);
        Integer discount = input.nextInt();
        ProductOrderPK pk = new ProductOrderPK(productId, orderId);
        ProductOrder entity = new ProductOrder(pk, productQuantity, discount);
        return entity;
    }

    public void createProductOrderEntity() throws SQLException {
        ProductOrder entity = setProductOrderInfo();
        int count = productOrderService.create(entity);
        if (count > 0) {
            logger.info(CREATED_ROWS_MESSAGE);
        } else {
            logger.info(ENTITY_WAS_NOT_CREATED_EXC);
        }
    }

    public void updateProductOrderEntity() throws SQLException {
        logger.info(ENTER_PRODUCT_QUANTITY_MESSAGE);
        Integer productQuantity = input.nextInt();
        logger.info(ENTER_DISCOUNT_MESSAGE);
        Integer discount = input.nextInt();
        logger.info(ENTER_PRODUCT_ID_MESSAGE);
        Integer productId = input.nextInt();
        logger.info(ENTER_ORDER_ID_MESSAGE);
        Integer orderId = input.nextInt();
        ProductOrderPK pk = new ProductOrderPK(productId, orderId);
        ProductOrder entity = new ProductOrder(pk, productQuantity, discount);
        int count = productOrderService.update(entity);
        if (count > 0) {
            logger.info(UPDATED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void selectAllProductOrderEntities() throws SQLException {
        logger.info(String.format(PRODUCT_CONTROLLER_STRING_FORMAT, PRODUCT_ID, ORDER_ID, PRODUCT_QUANTITY,
                DISCOUNT_PERCENT, PRODUCT_COUNTRY));
        List<ProductOrder> productOrderList = productOrderService.findAll();
        productOrderList.stream().forEach(System.out::println);
    }

    public void findAverageProductsInOrderQuantity() throws SQLException {
        logger.info(String.format(PRODUCT_ORDER_CONTROLLER_STRING_FORMAT, PRODUCT_ID, ORDER_ID, PRODUCT_QUANTITY,
                DISCOUNT_PERCENT));
        List<ProductOrder> products = productOrderService.findAverageProductsInOrderQuantity();
        products.stream().forEach(System.out::println);
    }
}

