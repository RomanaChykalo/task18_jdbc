package com.chykalo.shop.controller;

import com.chykalo.shop.exceptions.NoSuchEntityException;
import com.chykalo.shop.model.entities.Order;
import com.chykalo.shop.service.interfaces.OrderService;
import com.chykalo.shop.service.implementation.OrderServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_DO_NOT_EXIST_EXC;
import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_WAS_NOT_CREATED_EXC;
import static com.chykalo.shop.model.consts.InfoConsts.*;
import static com.chykalo.shop.model.consts.controller.OrderControllerConsts.*;
import static com.chykalo.shop.model.consts.controller.UserControllerConsts.*;

public class OrderController {
    private static Logger logger = LogManager.getLogger(OrderController.class);
    private static Scanner input = new Scanner(System.in);
    OrderService orderService = new OrderServiceImpl();


    public void deleteOrderById() throws SQLException {
        logger.info(SEARCH_ORDER_BY_ID_MESSAGE);
        Integer id = input.nextInt();
        int count = orderService.delete(id);
        if (count > 0) {
            logger.info(DELETED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void createOrder() throws SQLException {
        logger.info(ENTER_ORDER_ID_MESSAGE);
        Integer orderId = input.nextInt();
        logger.info(ENTER_ORDER_AMOUNT_MESSAGE);
        Integer orderAmount = input.nextInt();
        logger.info(ENTER_USER_ID_MESSAGE);
        Integer userId = input.nextInt();
        logger.info(ENTER_ORDER_DATE_MESSAGE);
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        input.nextLine();
        String line = input.nextLine();
        Date parsed = null;
        try {
            parsed = format.parse(line);
        } catch (ParseException e) {
            logger.info(e.getMessage());
        }
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        Order order = new Order(orderId, orderAmount, userId, sql);
        int count = orderService.create(order);
        if (count > 0) {
            logger.info(CREATED_ROWS_MESSAGE);
        } else {
            logger.info(ENTITY_WAS_NOT_CREATED_EXC);
        }
    }

    public void updateOrder() throws SQLException, ParseException {
        logger.info(ENTER_USER_ID_MESSAGE);
        Integer userId = input.nextInt();
        logger.info(ENTER_ORDER_AMOUNT_MESSAGE);
        Integer orderAmount = input.nextInt();
        logger.info(ENTER_ORDER_DATE_MESSAGE);
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        input.nextLine();
        String line = input.nextLine();
        Date parsed = format.parse(line);
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        logger.info(ENTER_ORDER_ID_MESSAGE);
        Integer orderId = input.nextInt();
        Order order = new Order(orderId, orderAmount, userId, sql);
        int count = orderService.update(order);
        if (count > 0) {
            logger.info(UPDATED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void findAllOrders() throws SQLException {
        logger.info(String.format(ORDER_CONTROLLER_STRING_FORMAT, ORDER_ID, ORDER_AMOUNT, USER_ID,
                ORDER_DATE));
        List<Order> orders = orderService.findAll();
        orders.stream().forEach(System.out::println);
    }

    public void findOrderByID() throws SQLException {
        logger.info(ENTER_ORDER_ID_MESSAGE);
        Integer id = input.nextInt();
        Order order = orderService.findById(id);
        if (order.getId() != 0) {
            logger.info(String.format(ORDER_CONTROLLER_STRING_FORMAT, ORDER_ID, ORDER_AMOUNT, USER_ID,
                    ORDER_DATE));
            Stream.of(order).forEach(System.out::println);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }
}
