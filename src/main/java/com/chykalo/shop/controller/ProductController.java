package com.chykalo.shop.controller;

import com.chykalo.shop.exceptions.*;
import com.chykalo.shop.model.entities.Product;
import com.chykalo.shop.service.interfaces.ProductService;
import com.chykalo.shop.service.implementation.ProductServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.chykalo.shop.model.consts.ExceptionMessage.*;
import static com.chykalo.shop.model.consts.InfoConsts.*;
import static com.chykalo.shop.model.consts.controller.ProductControllerConsts.*;
import static com.chykalo.shop.model.consts.controller.UserControllerConsts.*;

public class ProductController {
    private static Logger logger = LogManager.getLogger(ProductController.class);
    private static Scanner input = new Scanner(System.in);
    ProductService productService = new ProductServiceImpl();

    public void findAllProducts() throws SQLException {
        logger.info(String.format(PRODUCT_CONTROLLER_STRING_FORMAT, PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE,
                PRODUCT_COUNTRY));
        List<Product> products = productService.findAll();
        products.stream().forEach(System.out::println);
    }

    public void findProductExpensiveThenFifty() throws SQLException {
        logger.info(String.format(PRODUCT_CONTROLLER_STRING_FORMAT, PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE,
                PRODUCT_COUNTRY));
        List<Product> products = productService.findByPriceHigherThen50();
        products.stream().forEach(System.out::println);

    }

    public void findProductWithMaxPrice() throws SQLException {
        List<Product> products = productService.findProductWithMaxPrice();
        products.stream().forEach(System.out::println);
    }

    public void findProductCheaperThenFifty() throws SQLException {
        logger.info(String.format(PRODUCT_CONTROLLER_STRING_FORMAT, PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE,
                PRODUCT_COUNTRY));
        List<Product> products = productService.findByPriceLowerThen50();
        products.stream().forEach(System.out::println);
    }

    private List setProductInfo() {
        logger.info(ENTER_PRODUCT_PRICE_MESSAGE);
        BigDecimal productPrice = input.nextBigDecimal();
        logger.info(ENTER_PRODUCT_COUNTRY_FROM_MESSAGE);
        input.nextLine();
        String productCountry = input.nextLine();
        logger.info(ENTER_PRODUCT_ID_MESSAGE);
        Integer productId = input.nextInt();
        List productInfo = new ArrayList();
        productInfo.add(productPrice);
        productInfo.add(productCountry);
        productInfo.add(productId);
        return productInfo;
    }

    public void createProduct() throws SQLException {
        logger.info(ENTER_PRODUCT_NAME_MESSAGE);
        String productName = input.nextLine();
        List productInfo = setProductInfo();
        Product entity = new Product(productName, (BigDecimal) productInfo.get(0), (String) productInfo.get(1),
                (Integer) productInfo.get(2));
        int count = productService.create(entity);
        if (count > 0) {
            logger.info(CREATED_ROWS_MESSAGE);
        } else {
            throw new DuplicateEntityException(ENTITY_WAS_NOT_CREATED_EXC);
        }
    }

    public void updateProduct() throws SQLException {
        logger.info(ENTER_PRODUCT_NAME_MESSAGE);
        input.nextLine();
        String productName = input.nextLine();
        List productInfo = setProductInfo();
        Product entity = new Product(productName, (BigDecimal) productInfo.get(0), (String) productInfo.get(1),
                (Integer) productInfo.get(2));
        int count = productService.update(entity);
        if (count > 0) {
            logger.info(UPDATED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void deleteProduct() throws SQLException {
        logger.info(ENTER_PRODUCT_ID_MESSAGE);
        Integer id = input.nextInt();
        int count = productService.delete(id);
        if (count > 0) {
            logger.info(DELETED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }
}
