package com.chykalo.shop.controller;

import com.chykalo.shop.exceptions.DuplicateEntityException;
import com.chykalo.shop.exceptions.NoSuchEntityException;
import com.chykalo.shop.model.entities.User;
import com.chykalo.shop.service.interfaces.UserService;
import com.chykalo.shop.service.implementation.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_DO_NOT_EXIST_EXC;
import static com.chykalo.shop.model.consts.ExceptionMessage.ENTITY_WAS_NOT_CREATED_EXC;
import static com.chykalo.shop.model.consts.InfoConsts.*;
import static com.chykalo.shop.model.consts.controller.UserControllerConsts.*;

public class UserController {
    private static Logger logger = LogManager.getLogger(UserController.class);
    private static Scanner input = new Scanner(System.in);
    private UserService userService = new UserServiceImpl();

    public void findAllUsers() throws SQLException {
        logger.info(String.format(USER_CONTRL_STRING_FORMAT, USER_ID, USER_NAME, USER_SURNAME, USER_EMAIL, USER_PHONE));
        List<User> users = userService.findAll();
        users.stream().forEach(System.out::println);
    }

    public void findUserById() throws SQLException {
        logger.info(SEARCH_USER_BY_ID_MESSAGE);
        Integer id = input.nextInt();
        User user = userService.findById(id);
        if (user.getId() != 0) {
            logger.info(user);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void findUserByName() throws SQLException {
        logger.info(SEARCH_USER_BY_NAME_MESSAGE);
        String name = input.nextLine();
        List<User> users = userService.findByName(name);
        if (users.size() > 0) {
            users.stream().forEach(System.out::println);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void findUserBySurname() throws SQLException {
        logger.info(SEARCH_USER_BY_SURNAME_MESSAGE);
        String surname = input.nextLine();
        List<User> users = userService.findBySurname(surname);
        if (users.size() > 0) {
            users.stream().forEach(System.out::println);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void createUser() throws SQLException {
        User entity = setUserInfo();
        int count = userService.create(entity);
        if (count > 0) {
            logger.info(CREATED_ROWS_MESSAGE);
        } else {
            throw new DuplicateEntityException(ENTITY_WAS_NOT_CREATED_EXC);
        }
    }

    private User setUserInfo() {
        logger.info(ENTER_USER_ID_MESSAGE);
        Integer id = input.nextInt();
        logger.info(ENTER_USER_NAME_MESSAGE);
        input.nextLine();
        String userName = input.nextLine();
        logger.info(ENTER_USER_SURNAME_MESSAGE);
        String userSurname = input.nextLine();
        logger.info(ENTER_USER_EMAIL_MESSAGE);
        String userEmail = input.nextLine();
        logger.info(ENTER_USER_PHONE_MESSAGE);
        String userPhone = input.nextLine();
        User entity = new User(id, userName, userSurname, userEmail, userPhone);
        return entity;
    }

    public void updateUser() throws SQLException {
        logger.info(ENTER_USER_NAME_MESSAGE);
        String userName = input.nextLine();
        logger.info(ENTER_USER_SURNAME_MESSAGE);
        String userSurname = input.nextLine();
        logger.info(ENTER_USER_EMAIL_MESSAGE);
        String userEmail = input.nextLine();
        logger.info(ENTER_USER_PHONE_MESSAGE);
        String userPhone = input.nextLine();
        logger.info(ENTER_USER_ID_MESSAGE);
        Integer id = input.nextInt();
        User entity = new User(id, userName, userSurname, userEmail, userPhone);
        int count = userService.update(entity);
        if (count > 0) {
            logger.info(UPDATED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }

    public void deleteUser() throws SQLException {
        logger.info(ENTER_USER_ID_MESSAGE);
        Integer id = input.nextInt();
        int count = userService.delete(id);
        if (count > 0) {
            logger.info(DELETED_ROWS_MESSAGE);
        } else {
            throw new NoSuchEntityException(ENTITY_DO_NOT_EXIST_EXC);
        }
    }
}
