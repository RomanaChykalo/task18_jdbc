package com.chykalo.shop.dao.interfaces;

import com.chykalo.shop.dao.interfaces.GeneralDao;
import com.chykalo.shop.model.entities.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao extends GeneralDao<User,Integer> {
    List<User> findBySurname(String surname) throws SQLException;
    List<User> findByName(String name) throws SQLException;
}
