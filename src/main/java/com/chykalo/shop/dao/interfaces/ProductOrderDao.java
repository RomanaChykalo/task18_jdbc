package com.chykalo.shop.dao.interfaces;

import com.chykalo.shop.dao.interfaces.GeneralDao;
import com.chykalo.shop.model.entities.ProductOrder;
import com.chykalo.shop.model.entities.ProductOrderPK;

import java.sql.SQLException;
import java.util.List;

public interface ProductOrderDao extends GeneralDao<ProductOrder, ProductOrderPK> {
    List<ProductOrder> findAverageProductsInOrderQuantity() throws SQLException;
}
