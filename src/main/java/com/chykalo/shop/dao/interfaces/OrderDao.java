package com.chykalo.shop.dao.interfaces;

import com.chykalo.shop.dao.interfaces.GeneralDao;
import com.chykalo.shop.model.entities.Order;


public interface OrderDao extends GeneralDao<Order, Integer> {
}