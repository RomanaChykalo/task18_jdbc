package com.chykalo.shop.dao.interfaces;

import com.chykalo.shop.model.entities.Product;

import java.sql.SQLException;
import java.util.List;

public interface ProductDao extends GeneralDao<Product, Integer> {
    List<Product> findByPriceLowerThen50() throws SQLException;
    List<Product> findByPriceHigherThen50() throws SQLException;
    List<Product> findProductWithMaxPrice() throws SQLException;
}
