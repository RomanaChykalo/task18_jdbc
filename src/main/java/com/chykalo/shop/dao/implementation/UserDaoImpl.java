package com.chykalo.shop.dao.implementation;

import com.chykalo.shop.dao.interfaces.UserDao;
import com.chykalo.shop.model.entities.User;
import com.chykalo.shop.persistant.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.chykalo.shop.transformer.Transformer;

import static com.chykalo.shop.model.consts.dao.DaoConsts.*;

public class UserDaoImpl implements UserDao {
    Connection connection = ConnectionManager.getConnection();
    List<User> users = new ArrayList<>();

    private List<User> findByStringProperty(String userProperty, String query) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, userProperty);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    users.add((User) new Transformer(User.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return users;
    }

    @Override
    public List<User> findBySurname(String surname) throws SQLException {
        return findByStringProperty(surname, USER_FIND_BY_SURNAME);
    }

    @Override
    public List<User> findByName(String name) throws SQLException {
        return findByStringProperty(name, USER_FIND_BY_NAME);
    }

    @Override
    public List<User> findAll() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL_USERS)) {
                while (resultSet.next()) {
                    users.add((User) new Transformer(User.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return users;
    }

    @Override
    public User findById(Integer id) throws SQLException {
        return findByStringProperty(id.toString(), USER_FIND_BY_ID).get(0);
    }

    @Override
    public int create(User entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(CREATE_USER)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setString(3, entity.getSurname());
            ps.setString(4, entity.getEmail());
            ps.setString(5, entity.getPhone());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(User entity) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_USER)) {
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getSurname());
            ps.setString(3, entity.getEmail());
            ps.setString(4, entity.getPhone());
            ps.setInt(5, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_USER)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}

