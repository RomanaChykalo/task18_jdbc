package com.chykalo.shop.dao.implementation;

import com.chykalo.shop.dao.interfaces.ProductOrderDao;
import com.chykalo.shop.model.entities.ProductOrder;
import com.chykalo.shop.model.entities.ProductOrderPK;
import com.chykalo.shop.persistant.ConnectionManager;
import com.chykalo.shop.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.chykalo.shop.model.consts.dao.DaoConsts.*;

public class ProductOrderDaoImpl implements ProductOrderDao {
    Connection connection = ConnectionManager.getConnection();
    List<ProductOrder> productInOrder = new ArrayList<>();

    private List<ProductOrder> findByQuery(String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(query)) {
                while (resultSet.next()) {
                    productInOrder.add((ProductOrder) new Transformer(ProductOrder.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return productInOrder;
    }

    @Override
    public List<ProductOrder> findAll() throws SQLException {
        return findByQuery(FIND_ALL_PRODUCT_ORDER_ENTITIES);
    }

    public List<ProductOrder> findAverageProductsInOrderQuantity() throws SQLException {
        return findByQuery(FIND_PRODUCT_AVERAGE_QUANTITY);
    }

    @Override
    public int create(ProductOrder entity) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE_PRODUCT_ORDER_ENTITY)) {
            setProductOrderItemInfo(entity, preparedStatement);
            return preparedStatement.executeUpdate();
        }
    }

    private void setProductOrderItemInfo(ProductOrder entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, entity.getProductOrderPK().getProductId());
        preparedStatement.setInt(2, entity.getProductOrderPK().getOrderId());
        preparedStatement.setInt(3, entity.getProductQuantity());
        preparedStatement.setInt(4, entity.getDiscountPercent());
    }

    @Override
    public int update(ProductOrder entity) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PRODUCT_ORDER_ENTITY)) {
            preparedStatement.setInt(1, entity.getProductQuantity());
            preparedStatement.setInt(2, entity.getDiscountPercent());
            preparedStatement.setInt(3, entity.getProductOrderPK().getProductId());
            preparedStatement.setInt(4, entity.getProductOrderPK().getOrderId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(ProductOrderPK id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE_PRODUCT_ORDER_ENTITY)) {
            ps.setInt(1, id.getProductId());
            ps.setInt(2, id.getOrderId());
            return ps.executeUpdate();
        }
    }

    @Override
    public ProductOrder findById(ProductOrderPK productOrderPK) throws SQLException {
        return null;
    }
}
