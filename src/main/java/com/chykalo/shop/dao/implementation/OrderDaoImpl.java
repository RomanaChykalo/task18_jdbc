package com.chykalo.shop.dao.implementation;

import com.chykalo.shop.dao.interfaces.OrderDao;
import com.chykalo.shop.model.entities.Order;
import com.chykalo.shop.persistant.ConnectionManager;
import com.chykalo.shop.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.chykalo.shop.model.consts.dao.DaoConsts.*;

public class OrderDaoImpl implements OrderDao {
    Connection connection = ConnectionManager.getConnection();

    @Override
    public List<Order> findAll() throws SQLException {
        List<Order> orders = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL_ORDERS)) {
                while (resultSet.next()) {
                    orders.add((Order) new Transformer(Order.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return orders;
    }

    @Override
    public Order findById(Integer id) throws SQLException {
        Order order = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(ORDER_FIND_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    order = (Order) new Transformer(Order.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return order;
    }

    @Override
    public int create(Order order) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE_ORDER)) {
            preparedStatement.setInt(1, order.getId());
            preparedStatement.setInt(2, order.getAmount());
            preparedStatement.setInt(3, order.getUser_id());
            preparedStatement.setDate(4, order.getDate());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Order order) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ORDER)) {
            setOrderInfo(order, preparedStatement);
            return preparedStatement.executeUpdate();
        }
    }

    private void setOrderInfo(Order order, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, order.getUser_id());
        preparedStatement.setInt(2, order.getAmount());
        preparedStatement.setDate(3, order.getDate());
        preparedStatement.setInt(4, order.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ORDER)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}

