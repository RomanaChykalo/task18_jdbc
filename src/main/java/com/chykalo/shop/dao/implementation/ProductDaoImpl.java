package com.chykalo.shop.dao.implementation;

import com.chykalo.shop.dao.interfaces.ProductDao;
import com.chykalo.shop.model.entities.Product;
import com.chykalo.shop.persistant.ConnectionManager;
import com.chykalo.shop.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.chykalo.shop.model.consts.dao.DaoConsts.*;

public class ProductDaoImpl implements ProductDao {
    Connection connection = ConnectionManager.getConnection();
    List<Product> products = new ArrayList<>();

    @Override
    public List<Product> findByPriceLowerThen50() throws SQLException {
        return findByQuery(FIND_PRODUCTS_PRICE_LOWER_THEN_50);
    }

    private List<Product> findByQuery(String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(query)) {
                while (resultSet.next()) {
                    products.add((Product) new Transformer(Product.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return products;
    }

    @Override
    public List<Product> findByPriceHigherThen50() throws SQLException {
        return findByQuery(FIND_PRODUCTS_PRICE_HIGHER_THEN_50);
    }

    @Override
    public List<Product> findProductWithMaxPrice() throws SQLException {
        return findByQuery(FIND_PRODUCTS_MAX_PRICE);
    }

    @Override
    public List<Product> findAll() throws SQLException {
        return findByQuery(FIND_ALL_PRODUCTS);
    }

    @Override
    public Product findById(Integer integer) throws SQLException {
        return null;
    }

    @Override
    public int create(Product entity) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE_PRODUCT)) {
            setProductInfo(entity, preparedStatement);
            return preparedStatement.executeUpdate();
        }
    }

    private void setProductInfo(Product entity, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setBigDecimal(2, entity.getPrice());
        preparedStatement.setString(3, entity.getCountry());
        preparedStatement.setInt(4, entity.getId());
    }

    @Override
    public int update(Product entity) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PRODUCT)) {
            setProductInfo(entity, preparedStatement);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_PRODUCT)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}