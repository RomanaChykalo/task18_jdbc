SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZEVISIBLE ,RO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema shop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `shop` DEFAULT CHARACTER SET utf8 ;
USE `shop` ;

-- -----------------------------------------------------
-- Table `shop`.`product`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `shop`.`product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` DECIMAL(50) NOT NULL,
  `country` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `shop`.`user`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `shop`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT 'null',
  `surname` VARCHAR(45) NULL DEFAULT 'null',
  `email` VARCHAR(45) NULL,
  `phone` VARCHAR(11) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `shop`.`order`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `shop`.`order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `amount` INT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`id`)
    REFERENCES `shop`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
alter table `shop`.`order` add column `created` date;

-- -----------------------------------------------------
-- Table `shop`.`product_order`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `shop`.`product_order` (
  `product_id` INT(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`order_id`),
  KEY `fk_product_id_idx` (`product_id`),
  KEY `fk_order_id_idx` (`order_id`),
  CONSTRAINT `fk_product_id_idx`
    FOREIGN KEY (`product_id`)
    REFERENCES `shop`.`product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_id_idx`
    FOREIGN KEY (`order_id`)
    REFERENCES `shop`.`order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
alter table `shop`.`product_order` add column `product_quantity` int (11), add column `discount_percent` int (11) null;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- -----------------------------------------------------
-- Data for table `shop`.`product`
-- -----------------------------------------------------
START TRANSACTION;
USE `shop`;
LOCK TABLES `product` WRITE;
INSERT INTO `shop`.`product` (`id`, `name`, `price`, `country`) VALUES (DEFAULT, 'beer', 10, 'Ukraine');
INSERT INTO `shop`.`product` (`id`, `name`, `price`, `country`) VALUES (DEFAULT, 'chocolate', 20, 'Poland');
INSERT INTO `shop`.`product` (`id`, `name`, `price`, `country`) VALUES (DEFAULT, 'bread', 10, 'Ukraine');
INSERT INTO `shop`.`product` (`id`, `name`, `price`, `country`) VALUES (DEFAULT, 'coffee', 50, 'Poland');
COMMIT;
UNLOCK TABLES;

-- -----------------------------------------------------
-- Data for table `shop`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `shop`;
LOCK TABLES `user` WRITE;
INSERT INTO `shop`.`user` (`id`, `name`, `surname`, `email`, `phone`) VALUES (DEFAULT, 'Olena', 'Melnyk', 'info@gmail.com', '38097758767');
INSERT INTO `shop`.`user` (`id`, `name`, `surname`, `email`, `phone`) VALUES (DEFAULT, 'Petro', 'Chykalo', 'petryk@ukr.net', '38067340579');
INSERT INTO `shop`.`user` (`id`, `name`, `surname`, `email`, `phone`) VALUES (DEFAULT, 'Maxym', 'Manko', 'klymko123@gmail.com', '38098568402');
INSERT INTO `shop`.`user` (`id`, `name`, `surname`, `email`, `phone`) VALUES (DEFAULT, 'Diana', 'Vikhot', 'vihot@ukr.net', '38063595839');
COMMIT;
UNLOCK TABLES;
-- -----------------------------------------------------
-- Data for table `shop`.`order`
-- -----------------------------------------------------
START TRANSACTION;
USE `shop`;
LOCK TABLES `shop`.`order` WRITE;
INSERT INTO `shop`.`order` (`id`, `amount`, `user_id`,`created`) VALUES (5, 2, 3,'20170303');
INSERT INTO `shop`.`order` (`id`, `amount`, `user_id`,`created`) VALUES (6, 1, 2,'20180303');
INSERT INTO `shop`.`order` (`id`, `amount`, `user_id`,`created`) VALUES (7, 3, 4,'20151111');
INSERT INTO `shop`.`order` (`id`, `amount`, `user_id`,`created`) VALUES (8, 1, 1,'20011212');
COMMIT;
UNLOCK TABLES;

START TRANSACTION;
USE `shop`;
LOCK TABLES `product_order` WRITE;
INSERT INTO `shop`.`product_order` VALUES (6,2);
INSERT INTO `shop`.`product_order` VALUES (2,1);
INSERT INTO `shop`.`product_order` VALUES (3,2);
INSERT INTO `shop`.`product_order` VALUES (3,1);
COMMIT;
UNLOCK TABLES;
